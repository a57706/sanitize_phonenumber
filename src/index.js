const http = require('http');
var app = require('./app');
const server = http.createServer(app);

const port = process.env.PORT || 3000;
server.listen(port, '0.0.0.0', () => console.log(`server listening on port ${port}`));