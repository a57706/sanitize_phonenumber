const express = require('express');
const cors = require('cors')
const app = express();

app.use(cors());
app.use(express.json({ limit: '12mb' }));
app.use(express.urlencoded({ limit: '12mb', extended: true }));

var router = express.Router();
router.post('/sanitize/phonenumber/:phone', (req, res) => {
    const numberPattern = /\d+/g;
    const { phone } = req.params;
    const result = parseInt(phone.match(numberPattern).join(''));
    res.status(200).json(result);
});
// app.use('/', (req, res) => res.status(200).send('OK'));
// app.use('/health', (req, res) => res.status(200).send('OK'));
app.use('/v1', router);

module.exports = app;